Application Name: PC Store

Description:  A simple web computer store that list computers with their details including price and category. It also have a very simple PayPal buy button on the details page.

Installation:

Step 1:

Install nodejs into your computer.

Step 1:

Run "npm install" to your root directory.

Step 2:

Follow the steps for the installation of Angular Seed Boiler Plate

https://github/angular/angular-seed

Step 2:

Remove the app folder of your angular seed folder and clone this repository to the root of your folder.

Step 3:

Run "npm start" and visit the url http://localhost:8000.