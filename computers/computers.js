angular.module('pcStore.computers', ['ngRoute'])

.config(['$routeProvider', function($routeProvider){
	$routeProvider.
		when('/computers', {
			templateUrl: 'computers/computers.html',
			controller: 'ComputerCtrl'
		}).
		when('/computer/:computerId', {
			templateUrl: 'computers/computer-details.html',
			controller: 'ComputerDetailsCtrl'
		});
}])

.controller('ComputerCtrl', ['$scope', '$http', function($scope, $http){
	$http.get('json/computers.json').success(function(data){
		$scope.computers = data;
	});
}])

.controller('ComputerDetailsCtrl', ['$scope', '$routeParams', '$http', '$filter', function($scope, $routeParams, $http, $filter){
	var computerId = $routeParams.computerId;
	$http.get('json/computers.json').success(function(data){
		$scope.computer = $filter('filter')(data, function(d){
			return d.id == computerId;
		})[0];
		$scope.mainImage = $scope.computer.images[0].name;
	});

	$scope.setImage = function(image){
		$scope.mainImage = image.name;
	}
}]);