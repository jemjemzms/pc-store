'use strict';

// Declare app level module which depends on views, and components
angular.module('pcStore', [
  'ngRoute',
  'pcStore.computers'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/computers'});
}]);
